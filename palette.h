#ifndef PALETTE_H
#define PALETTE_H

// stl
#include <vector>

#ifdef MAPNIK_BIG_ENDIAN
#define U2RED(x) (((x)>>24)&0xff)
#define U2GREEN(x) (((x)>>16)&0xff)
#define U2BLUE(x) (((x)>>8)&0xff)
#define U2ALPHA(x) ((x)&0xff)
#else
/*
#define U2RED(x) ((x)&0xff)
#define U2GREEN(x) (((x)>>8)&0xff)
#define U2BLUE(x) (((x)>>16)&0xff)
#define U2ALPHA(x) (((x)>>24)&0xff)
*/

#define U2BLUE(x) ((x)&0xff)
#define U2GREEN(x) (((x)>>8)&0xff)
#define U2RED(x) (((x)>>16)&0xff)
#define U2ALPHA(x) (((x)>>24)&0xff)


#endif


//typedef png_by byte;
typedef unsigned char byte;
struct rgba;

struct rgb {
    byte r;
    byte g;
    byte b;

    inline rgb(byte r_, byte g_, byte b_) : r(r_), g(g_), b(b_) {}
    rgb(rgba const& c);

    inline bool operator==(const rgb& y) const
    {
        return r == y.r && g == y.g && b == y.b;
    }
};

struct rgbn {
    byte r;
    byte g;
    byte b;
	int n;

    inline rgbn(byte r_, byte g_, byte b_, int n_) : r(r_), g(g_), b(b_), n(n_) {}

    inline bool operator==(const rgbn& y) const
    {
        return r == y.r && g == y.g && b == y.b;
    }
	inline bool operator>(const rgbn& y) const
    {
        return n > y.n;
    }
	inline bool operator<(const rgbn& y) const
    {
        return n < y.n;
    }
};


struct rgba
{
    byte r;
    byte g;
    byte b;
    byte a;

    inline rgba(byte r_, byte g_, byte b_, byte a_)
        : r(r_),
          g(g_),
          b(b_),
          a(a_) {}

    inline rgba(rgb const& c)
        : r(c.r),
          g(c.g),
          b(c.b),
          a(0xFF) {}

    inline rgba(unsigned const& c)
        : r(U2RED(c)),
          g(U2GREEN(c)),
          b(U2BLUE(c)),
          a(U2ALPHA(c)) {}

    inline bool operator==(const rgba& y) const
    {
        return r == y.r && g == y.g && b == y.b && a == y.a;
    }

    // ordering by mean(a,r,g,b), a, r, g, b
    struct mean_sort_cmp
    {
        bool operator() (const rgba& x, const rgba& y) const;
    };

};

class rgba_palette {
public:
    enum palette_type { PALETTE_RGBA = 0, PALETTE_RGB = 1, PALETTE_ACT = 2 };

    explicit rgba_palette(std::string const& pal, palette_type type = PALETTE_RGBA);
    rgba_palette();

    const std::vector<rgb>& palette() const;
    const std::vector<unsigned>& alphaTable() const;

    //unsigned char quantize(unsigned c) const;

    bool valid() const;
    std::string to_string() const;

private:
    //void parse(std::string const& pal, palette_type type);

private:
    std::vector<rgba> sorted_pal_;
    //mutable rgba_hash_table color_hashmap_;

    unsigned colors_;
    std::vector<rgb> rgb_pal_;
    std::vector<unsigned> alpha_pal_;
};

#endif // PALETTE_H

