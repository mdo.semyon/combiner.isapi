// Combiner.cpp : Defines the entry point for the DLL application.

//�����������
#define UNICODE
#define _WIN32_WINNT					0x0500
#define WINVER							0x0500

#define ERR_WRONG_NUMBER_OF_DIGITS   1
#define ERR_NOT_A_MASTERCARD         2
#define ERR_INVALID_CC               3
#define ERR_INVALID_INPUT            4

//����������� ��� ���������������
#define DefaultTimeout 60000
#define DefaultSize 10
#define DefaultPool 20

#define VERSION_MINOR 0
#define VERSION_MAJOR 1

#include "IsapiTools.h"
#include "common.h"
#include "Config.h"
#include "Approximator.h"
#include "Utils.h"
#include "LibpngExt.h"
#include "helpers.h"

const char		szDescription[]="PST.Combiner ISAPI Extension";
HANDLE			hEvent = NULL;
HANDLE			hSemaphore = NULL;					//����� ��������
DWORD			dwMaxThreads = DefaultPool;			//������������ ����� �����
DWORD			dwQueueTimeout = DefaultTimeout;	//������� ���������� � �������
DWORD			dwQueueSize = DefaultSize;			//������ �������
HANDLE			hIOPort = NULL;
CComAutoCriticalSection g_cs;						//initialization synchronyzer
long			g_lInitCount(0);					//attached threads counter
BOOL			g_bInitResult(FALSE);
ULONG_PTR		m_gdiplusToken;						//��������������� ���������� ��� ��������� Gdiplus
GUID			PngId;								//HGLOBAL m_hBuffer;
Configuration*	config = new Configuration();
LibpngExt*		libpng;
CRITICAL_SECTION stream_lock;						//������ � �������
CRITICAL_SECTION config_lock;
std::map<int, CBitStream*> stream_map;
unsigned m_layersSize = 0;


void PNGAPI output_flush_fn(png_structp png_ptr)
{
}

void PNGAPI write_data_fn(png_structp png_ptr, png_bytep data, png_size_t length)
{
	::EnterCriticalSection(&stream_lock);
	int thread_id = ::GetCurrentThreadId();
	CBitStream* stream;
	std::map<int, CBitStream*>::iterator stream_it = stream_map.find(thread_id);
	if (stream_it != stream_map.end()) {
		stream = stream_it->second;
		stream->Write_png_bytep(data, length);
		//stream_map.erase(stream_it);
	}
	::LeaveCriticalSection(&stream_lock);
}

BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
    return TRUE;
}

BOOL WINAPI GetExtensionVersion(HSE_VERSION_INFO *pVer)
{
	//������������� ������
	pVer->dwExtensionVersion=MAKELONG(VERSION_MINOR, VERSION_MAJOR);
	//�������� ��������
	memcpy(pVer->lpszExtensionDesc, szDescription, sizeof(szDescription));
	CComCritSecLock<CComAutoCriticalSection> lock(g_cs);
	if (g_lInitCount++ > 0)
		return g_bInitResult;
	HKEY hkey;

	::InitializeCriticalSection(&config_lock);
	::InitializeCriticalSection(&stream_lock);

	//�������������� � ����
	//helpers::initLog();
	//������� ���������� ���������
	//config->readConfig("C:\\inetpub\\wwwroot", "combinerConfig.xml");

	libpng = new LibpngExt(write_data_fn, output_flush_fn);

	DWORD dwSize = 4;
	DWORD dwType;
	dwQueueTimeout = DefaultTimeout;
	dwMaxThreads = DefaultPool;
	dwQueueSize = DefaultSize;

	//������������� gdiplus
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&m_gdiplusToken, &gdiplusStartupInput, NULL);
	ReadCodecsList(true , &PngId);

	hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	//������� IO Completion port
	if(hIOPort = ::CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0))
	{
		//������� �������
		if(hSemaphore = ::CreateSemaphore(NULL, dwQueueSize, dwQueueSize, NULL))
		{
			for(DWORD i=0; i < dwMaxThreads; i++)
			if(_beginthread(ExtensionThreadProc, 0, NULL) == -1)
				//BOOST_LOG_SEV(lg, boost::log::trivial::error) << "Unable to create thread.";

			//������� ��������� �� �������� �������������
			//BOOST_LOG_SEV(lg, boost::log::trivial::info) << "CombinerISAPI successfully initialized.";
			g_bInitResult = TRUE;
			return TRUE;
		}
	}
	//������� ��������� � ����
	//BOOST_LOG_SEV(lg, boost::log::trivial::error) << "CombinerISAPI initialization failed.";
	return FALSE;
}

BOOL WINAPI TerminateExtension(DWORD dwFlags)
{
	CComCritSecLock<CComAutoCriticalSection> lock(g_cs);
	if (--g_lInitCount > 0)
		return TRUE;
	if(hIOPort)
	::CloseHandle(hIOPort);
	//����������� �������
	if(hSemaphore)
	::CloseHandle(hSemaphore);


	if(hEvent)
	{
		ResetEvent(hEvent);
		::CloseHandle(hEvent);
	}

	Gdiplus::GdiplusShutdown(m_gdiplusToken);
	//delete libpng;
	//���������� ��������� �� �������� ����������
	//BOOST_LOG_SEV(lg, boost::log::trivial::info) << "CombinerISAPI successfully uninitialized.";
	return TRUE;
}

DWORD WINAPI HttpExtensionProc(EXTENSION_CONTROL_BLOCK *pECB)
{
	//���� ������������ ���� ��� ����������� ��������
	if(::WaitForSingleObject(hSemaphore,dwQueueTimeout) == WAIT_TIMEOUT)
	{
		//����� �������
		//BOOST_LOG_SEV(lg, boost::log::trivial::info) << "ISAPI message queue timeout.";
		return HSE_STATUS_ERROR;
	}
	//������ ����� - ��������� ������
	
	//!!!! ������ ��� �����/������� ����������� ������ (
	if(!config->loaded || m_layersSize == 0)
	{
		::EnterCriticalSection(&config_lock);
		
		char pszURL[_MAX_PATH];   
		strcpy_s(pszURL, sizeof(pszURL), "/");   
		HSE_URL_MAPEX_INFO umi;   
		DWORD dwSize = static_cast<DWORD>(strlen(pszURL));   
		pECB->ServerSupportFunction(pECB->ConnID, HSE_REQ_MAP_URL_TO_PATH_EX, pszURL, &dwSize, reinterpret_cast<LPDWORD>(&umi)); 
		
		std::string path = std::string(umi.lpszPath);
		config->readConfig(path, "combinerConfig.xml");
		config->loaded = true;
		//delete[] pszURL;

		BOOL isOk = ::SetEvent(hEvent);
		if(!isOk) 
		{ 
			// ������ 
		}

		::LeaveCriticalSection(&config_lock);
	}
	
	

	//�������� ��������� �� Extension Control Block ������ ��������� �� overlapped
	if(!PostQueuedCompletionStatus(hIOPort, -1, NULL, (LPOVERLAPPED)pECB))
	{
		::ReleaseSemaphore(hSemaphore, 1, NULL);
		return HSE_STATUS_ERROR;
	}
	//���������� ������ ���������� ���������
	return HSE_STATUS_PENDING;
}

	//bool abc = false;
void ExtensionThreadProc(LPVOID)
{
	DWORD dwWaitResult = ::WaitForSingleObject(hEvent, INFINITE );
	if( dwWaitResult != WAIT_OBJECT_0 ) 
	{ 
		// ������ 
	}
	else
	{
		m_layersSize = config->Layers->size();
		::ResetEvent(hEvent);
		// ��� ������ ����� ���� ���������, ��� ������� ����� �������� �������������.
	}

	::EnterCriticalSection(&stream_lock);
	CBitStream* stream;
	int thread_id = ::GetCurrentThreadId();
	std::map<int, CBitStream*>::iterator stream_it = stream_map.find(thread_id);
	if (stream_it != stream_map.end()) {
		stream = stream_it->second;
	}
	else
	{
		stream = new CBitStream();
		std::pair<int, CBitStream*> spair(thread_id, stream);
		stream_map.insert(spair);
	}
	::LeaveCriticalSection(&stream_lock);

  DWORD dwFlag, dwNull;
  OVERLAPPED * pParam;

  while(::GetQueuedCompletionStatus(hIOPort, &dwFlag, &dwNull, &pParam, INFINITE) && dwFlag != NULL)
  {
    //����������� ���������
    LPEXTENSION_CONTROL_BLOCK lpECB = (LPEXTENSION_CONTROL_BLOCK)pParam;

	//������ �������� ����������
	std::string			str = std::string(lpECB->lpszQueryString);
	char*				cQuadkey;
	char*				cLayers;
	char*				pch;    
	std::vector<char*>	tokens;
	boost::char_separator<char> sep("=", "&", boost::keep_empty_tokens);
    tokenizer			query_tokens(str, sep);	

    for (tokenizer::iterator tok_iter = query_tokens.begin(); tok_iter != query_tokens.end(); ++tok_iter)
	{
		const char * tmp = (*tok_iter).c_str();
		if (strcmp(tmp, "layers") == 0) {
			if (tok_iter != query_tokens.end())
			{
				++tok_iter;
				cLayers = new char[(*tok_iter).size() + 1];
				std::copy((*tok_iter).begin(), (*tok_iter).end(), cLayers);
				cLayers[(*tok_iter).size()] = '\0';
			}
		}
		if (strcmp(tmp, "quadkey") == 0) {

			if (tok_iter != query_tokens.end())
			{
				++tok_iter;
				cQuadkey = new char[(*tok_iter).size() + 1];
				std::copy((*tok_iter).begin(), (*tok_iter).end(), cQuadkey);
				cQuadkey[(*tok_iter).size()] = '\0';
			}
		}
	}
	pch = strtok(cLayers,"-");
	tokens.push_back(pch);
	while (pch != NULL)
	{
		pch = strtok(NULL, "-");
		tokens.push_back(pch);
	}

	//std::string qqq = std::string(cQuadkey);
	//int found1 = qqq.find("111111");
	//if (found1 >= 0)
	//{
	//	abc = true;
	//	
	//	try
	//	{
	//		//BOOST_LOG_SEV(lg, boost::log::trivial::info) << "CombinerISAPI: Failed to register event source.";
	//	}
	//	catch(std::exception *e)
	//	{
	//	}

	//	
	//	//if(!config->loaded)
	//	//{
	//		char pszURL[_MAX_PATH];   
	//		strcpy_s(pszURL, sizeof(pszURL), "/");   
	//		HSE_URL_MAPEX_INFO umi;   
	//		DWORD dwSize = static_cast<DWORD>(strlen(pszURL));   
	//		lpECB->ServerSupportFunction(lpECB->ConnID, HSE_REQ_MAP_URL_TO_PATH_EX, pszURL, &dwSize, reinterpret_cast<LPDWORD>(&umi)); 
	//	
	//		std::string path = std::string(umi.lpszPath);
	//		config->readConfig(path, "combinerConfig.xml");
	//		config->loaded = true;
	//		//delete[] pszURL;
	//	//}
	//	
	//	
	//}
	
	//������� ������ � �������������� GDIplus
	int					tileX, 
						tileY, 
						levelOfDetail;
	Gdiplus::Bitmap		*m_pBitmap;
	wchar_t				*wc;
	Approximator		*m_approximator = new Approximator(config->blankTile);
	Gdiplus::RectF		*rectF = new Gdiplus::RectF(0, 0, 256, 256);
	Gdiplus::Bitmap		*compositeImage = new Gdiplus::Bitmap(256,256, PixelFormat32bppARGB);
	Gdiplus::Graphics	*compositeGraphics = Gdiplus::Graphics::FromImage(compositeImage);

	compositeGraphics->SetCompositingMode(Gdiplus::CompositingMode::CompositingModeSourceOver);

	std::string strQuadkey(cQuadkey);
	int idx = strQuadkey.find("_");
	std::string quadkey = strQuadkey.substr(0, idx);

	QuadKeyToTileXY(quadkey.c_str(), &tileX, &tileY, &levelOfDetail);
	//for(std::vector<char*>::reverse_iterator it = tokens.rbegin(); it != tokens.rend(); ++it)
	for(std::vector<char*>::iterator it = tokens.begin(); it != tokens.end(); ++it)
	{
		const char* t = *it;
		if (t != NULL)
		{
			std::string objectType_gso(t);
			idx = objectType_gso.find("_gso");
			if (idx > 0)
			{
				std::string objectTypeSysName = objectType_gso.substr(0, idx);
				std::string gso = objectType_gso.substr(idx + 1);

				std::stringstream ss;
				ss << config->generatedRastrTilesPath << "\\" << objectTypeSysName << "\\" << gso;

				std::string path = m_approximator->GetTilePath(ss.str(), tileX, tileY, levelOfDetail);
				if (boost::filesystem::exists(path))
				{
					//���� ������������ � ���� �����
					wc = utils::charToWChar((char*)path.c_str());
					m_pBitmap = new Gdiplus::Bitmap(wc, false);
					if(m_pBitmap)
					{
						if (m_pBitmap->GetLastStatus() == Gdiplus::Ok)
						{
							compositeGraphics->DrawImage(m_pBitmap, *rectF, 0, 0, 256, 256, Gdiplus::Unit::UnitPixel);
						}
						SAFE_DELETE(m_pBitmap);	
					}
					delete[] wc;
				}
			}
			else
			{
				for(std::vector<Layer>::iterator l = config->Layers->begin(); l != config->Layers->end(); ++l)
				{
					bool m_layerProcessed = false;

					if(levelOfDetail >= l->minZoom && levelOfDetail <= l->maxZoom)
					{
						idx = l->sysName.find(t);
						if (idx >= 0)
						{
							std::string path = m_approximator->GetTilePath(l->path, tileX, tileY, levelOfDetail);
							if (boost::filesystem::exists(path))
							{
								//���� ������������ � ���� �����
								wc = utils::charToWChar((char*)path.c_str());
								m_pBitmap = new Gdiplus::Bitmap(wc, false);
								if(m_pBitmap)
								{
									if (m_pBitmap->GetLastStatus() == Gdiplus::Ok)
									{
										compositeGraphics->DrawImage(m_pBitmap, *rectF, 0, 0, 256, 256, Gdiplus::Unit::UnitPixel);
									
										m_layerProcessed = true;
									}
									SAFE_DELETE(m_pBitmap);	
								}
								delete[] wc;
							}
					
							if (!m_layerProcessed && levelOfDetail > l->maxAvaluableZoom)
							{
								//�� ����� ���� � � ������������ ����� ������� ������������� �������������
								Gdiplus::Bitmap* targetImage = new Gdiplus::Bitmap(256, 256, PixelFormat32bppARGB);
								m_approximator->GetApproximatedTile(targetImage, cQuadkey, l->path, l->maxAvaluableZoom);
								if (targetImage)
								{
									compositeGraphics->DrawImage(targetImage, *rectF, 0, 0, 256, 256, Gdiplus::Unit::UnitPixel);
									SAFE_DELETE(targetImage);
									m_layerProcessed = true;
								}
								else
								{
									//�� ����� ���� � ������������� �� ������, ���������� ������ ����
									if (boost::filesystem::exists(config->blankTile))
									{
										wc = utils::charToWChar((char*)config->blankTile.c_str());
										targetImage = new Gdiplus::Bitmap(wc, false);
										if(targetImage)
										{
											if (targetImage->GetLastStatus() == Gdiplus::Ok)
											{
												compositeGraphics->DrawImage(targetImage, *rectF, 0, 0, 256, 256, Gdiplus::Unit::UnitPixel);
												m_layerProcessed = true;
											}
											SAFE_DELETE(targetImage);
										}
										delete[] wc;
									}
								}
							}
							break;
						}
					}
				}
			}
		}
	}

	//������������ ��������
	delete m_approximator;
	SAFE_DELETE(rectF);
	SAFE_DELETE(compositeGraphics);
	delete[] cQuadkey; 
	delete[] cLayers; 
	tokens.clear();

	//��������� �������� ����� � �������������� libpng
	png_bytep row_pointers[256];
	std::vector<rgb> palette;
	std::vector<unsigned> alpha;

	image_data_32 image_in(256, 256);
	Gdiplus::BitmapData bmpDataSource;
	Gdiplus::Rect* rect = new Gdiplus::Rect(0, 0, 256, 256);
	Gdiplus::Status status = ((Gdiplus::Bitmap*)compositeImage)->LockBits(rect, Gdiplus::ImageLockMode::ImageLockModeRead, compositeImage->GetPixelFormat(), &bmpDataSource);
	if (status == Gdiplus::Status::Ok)
	{
		if(config->use8bppIndexedConverter/* && abc*/)
		{
			libpng->readData(bmpDataSource.Scan0, image_in);
			libpng->reduceTo8bpp(image_in, &palette, &alpha, row_pointers);
			libpng->write8bppRGB(palette, alpha, row_pointers);
		}
		else
		{
			libpng->readDataT(bmpDataSource.Scan0, image_in);
			libpng->save_as_32bpp_png(image_in);
		}
	}
	((Gdiplus::Bitmap*)compositeImage)->UnlockBits(&bmpDataSource);
	//����������� GDIplus �������
	SAFE_DELETE(rect);
	SAFE_DELETE(compositeImage);
	
	//��������� ����� ��� ��������
	ISAPI_REQUEST* request = new ISAPI_REQUEST(lpECB);
	request->_ResponseBuffer = stream->GetStream();
	request->_ResponseBufferLength = stream->GetStreamTotalLength();
	request->AddHeaderToBufferedResponse("Cache-Control", "public");
	request->AddHeaderToBufferedResponse("Content-type", "image/webp");
	request->SyncTransmitBufferedResponse(300);
	request->~ISAPI_REQUEST();

	//����������� ����� ������
	free(stream->m_lpStream);
	stream->m_lpStream = NULL;
	stream->m_dwStreamLen = 0;
	stream->m_dwStreamOffset = 0;
	stream->m_dwCurrentPosition = 0;
	stream->m_CurrentBit = 0;

	if(config->use8bppIndexedConverter/* && abc*/)
	{
		palette.clear();
		alpha.clear();

		for (int row = 0; row < 256; row++)
			free(row_pointers[row]);

		//delete[] row_pointers;
	}

    //����������� �������
    ::ReleaseSemaphore(hSemaphore, 1, NULL);
  }

  _endthread();
}