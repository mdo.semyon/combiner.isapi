/*++

Copyright (c) 2003  Microsoft Corporation

Module Name: IsapiRequest.h

Abstract:

    A class to do common ISAPI extension
    processing tasks

Author:

    ISAPI developer (Microsoft employee), October 2002

--*/

#include <IsapiTools.h>

//
// Define the entity for a generic 500 server error response
//

#define SERVER_ERROR    \
    "<html>\r\n"                                         \
    "<head> <title>500 Server Error</title> </head>\r\n" \
    "<h1> Server Error </h1>\r\n"                        \
    "<hr>\r\n"                                           \
    "The server was unable to process your request.\r\n" \
    "</html>"

//
// Declare instance of static OS major version variable
//

DWORD   ISAPI_REQUEST::_dwOsMajorVersion = 0;

ISAPI_REQUEST::ISAPI_REQUEST(EXTENSION_CONTROL_BLOCK* pEcb)
    : _pEcb( pEcb ),
      _dwMaxSyncWriteSize( 0 ), // Use ISAPI_STRING default
      _fNeedDoneWithSession( FALSE ),
      _fClientIsConnected( TRUE ),
      _hImpersonationToken( NULL )
{
    //_ResponseBuffer.SetMaxAlloc( DEFAULT_RESPONSE_BUFFER_SIZE );

    _dwIISMajorVersion = pEcb->dwVersion >> 16;
    _dwIISMinorVersion = pEcb->dwVersion & 0xffff;
}

ISAPI_REQUEST::~ISAPI_REQUEST(VOID) {
    DWORD   dwState;

	if ( _fNeedDoneWithSession ) {
        // Since buffered responses always send a content-length,
        // we should go ahead and keep the connection open
        dwState = HSE_STATUS_SUCCESS_AND_KEEP_CONN;
        _pEcb->ServerSupportFunction( _pEcb->ConnID,
                                      HSE_REQ_DONE_WITH_SESSION,
                                      &dwState,
                                      NULL,
                                      NULL );
    }
}

BOOL ISAPI_REQUEST::SyncTransmitBufferedResponse(DWORD dwExpireSeconds) {
    CHAR            szContentLength[32];
    ISAPI_STRING    strExpires;
	ISAPI_STRING    strLastModified;
    BOOL            fIIS6Cacheable = FALSE;
    BOOL            fResult;

    // If a status has not been set, use the default
    if ( _StatusBuffer.QueryCCH() == 0 ) {
        fResult = _StatusBuffer.Copy( DEFAULT_STATUS );

        if ( !fResult ) {
            goto Failed;
        }
    }

    // Calculate the content-length and add the header.
    // This is required for keep-alive.
    itoa(_ResponseBufferLength, szContentLength, 10);
    fResult = AddHeaderToBufferedResponse( "Content-Length", szContentLength );
    if ( !fResult ) {
        goto Failed;
    }

	//Last-Modified
    FILETIME ft;
    if ( GetCurrentTimeAsFileTime( &ft ) == FALSE ) {
        goto Failed;
    }

	fResult = GetFileTimeAsString( &ft, &strLastModified );
	if ( !fResult ) {
		goto Failed;
    }

	if (AddHeaderToBufferedResponse( "Last-Modified", strLastModified.QueryStr() ) == FALSE ) {
		goto Failed;
    }

    // Add an expires header if specified
    if ( dwExpireSeconds != NO_EXPIRATION ) {
        if ( dwExpireSeconds == IMMEDIATE_EXPIRATION ) {
            fResult = strExpires.Copy( "0" );
        } else {
            FILETIME ft;
            if ( GetCurrentTimeAsFileTime( &ft ) == FALSE ) {
                goto Failed;
            }

            // Add the cache milliseconds.  Note that FILETIME
            // is a count of 100 nanosecond invervals, thus we
            // need to multiply by 10000000 to get seconds
			((LARGE_INTEGER*)&ft)->HighPart += dwExpireSeconds;// * 10000000;
            fResult = GetFileTimeAsString( &ft, &strExpires );

            fIIS6Cacheable = TRUE;
        }

        if ( !fResult ) {
            goto Failed;
        }

        if (AddHeaderToBufferedResponse( "Expires", strExpires.QueryStr() ) == FALSE ) {
            goto Failed;
        }
    }

    // Terminate the header string
    fResult = _HeaderBuffer.Append( "\r\n" );

    if ( !fResult ) {
        goto Failed;
    }

    if ( _dwIISMajorVersion > 5 ) {
        fResult = SyncSendResponseIIS6(fIIS6Cacheable);
    } else {
		goto Failed;
	}

    return fResult;
Failed:
    return FALSE;
}

BOOL ISAPI_REQUEST::SyncSendResponseIIS6(BOOL fCacheResponse) {
    HSE_VECTOR_ELEMENT  Element;
    HSE_RESPONSE_VECTOR Vector;
    BOOL                fResult;

    Element.ElementType = HSE_VECTOR_ELEMENT_TYPE_MEMORY_BUFFER;
    Element.pvContext = _ResponseBuffer;//.QueryPtr();
    Element.cbOffset = 0;
    Element.cbSize = _ResponseBufferLength;// .QueryDataSize();


    Vector.dwFlags = HSE_IO_SYNC | HSE_IO_FINAL_SEND | HSE_IO_SEND_HEADERS;
    if ( fCacheResponse ) {
        Vector.dwFlags |= HSE_IO_CACHE_RESPONSE;
    }

    Vector.pszStatus = _StatusBuffer.QueryStr();
    Vector.pszHeaders = _HeaderBuffer.QueryStr();
    Vector.nElementCount = 1;
    Vector.lpElementArray = &Element;


    _fNeedDoneWithSession = TRUE;
    fResult = _pEcb->ServerSupportFunction( _pEcb->ConnID,
                                            HSE_REQ_VECTOR_SEND,
                                            &Vector,
                                            NULL,
                                            NULL );

    if ( !fResult ) {
        _fNeedDoneWithSession = FALSE;
    }

    return fResult;

Failed:
	return FALSE;
}

BOOL ISAPI_REQUEST::SyncSendStatusAndHeaders(CHAR* szStatus, CHAR* szHeaders)
{
    DWORD   cchHeaders;
    DWORD   dwStatus;
    BOOL    fResult;

    // Set defaults if needed
    if ( szStatus == NULL ) {
        szStatus = DEFAULT_STATUS;
    }

    if ( szHeaders == NULL ) {
        szHeaders = DEFAULT_HEADERS;
    }

    // Make sure that headers are properly terminated
    cchHeaders = strlen( szHeaders );
    if ( strcmp( szHeaders, DEFAULT_HEADERS ) != 0 ) {
        if ( cchHeaders < 4 ) {
            SetLastError( ERROR_INVALID_PARAMETER );
            goto Failed;
        }

        if ( strcmp( szHeaders + cchHeaders - 4, "\r\n\r\n" ) != 0 ) {
            SetLastError( ERROR_INVALID_PARAMETER );
            goto Failed;
        }
    }

    dwStatus = atoi( szStatus );
    if ( dwStatus < 100 || dwStatus > 999 ) {
        SetLastError( ERROR_INVALID_PARAMETER );
        goto Failed;
    }

    _pEcb->dwHttpStatusCode = dwStatus;

    fResult =  _pEcb->ServerSupportFunction( _pEcb->ConnID,
                                             HSE_REQ_SEND_RESPONSE_HEADER,
                                             szStatus,
                                             NULL,
                                             (DWORD*)szHeaders );
    if ( !fResult ) {
        _fClientIsConnected = FALSE;

        goto Failed;
    }

    return TRUE;
Failed:
    return FALSE;
}

BOOL ISAPI_REQUEST::SyncWriteClientArgs(CHAR* szFormat, va_list args)
{
    ISAPI_STRING    IsapiString( _dwMaxSyncWriteSize );
    DWORD           cbData;
    BOOL            fResult;

    fResult = IsapiString.Vsprintf(szFormat, args);

    if ( !fResult ) {
        goto Failed;
    }

    cbData = IsapiString.QueryCCH();
    fResult = _pEcb->WriteClient( _pEcb->ConnID,
                                  IsapiString.QueryStr(),
                                  &cbData,
                                  HSE_IO_SYNC | HSE_IO_NODELAY );

    if ( !fResult )
    {
        _fClientIsConnected = FALSE;
    }

    return fResult;

Failed:
    return FALSE;
}

BOOL ISAPI_REQUEST::SyncWriteCompleteResponse(CHAR* szStatus, CHAR* szHeaders, CHAR* szFormat, ...)
{
    va_list args;
    BOOL    fResult;

    // Send the status and headers
    fResult = SyncSendStatusAndHeaders(szStatus, szHeaders);
    if ( !fResult ) {
        goto Failed;
    }

    // Send the entity
    va_start( args, szFormat );
    fResult = SyncWriteClientArgs(szFormat, args);

    va_end( args );
    return fResult;

Failed:
    return FALSE;
}

BOOL ISAPI_REQUEST::SetBufferedResponseStatus(CHAR* szStatus) {
    return _StatusBuffer.Copy( szStatus );
}

BOOL ISAPI_REQUEST::AddHeaderToBufferedResponse(CHAR* szName,CHAR* szValue) {
    ISAPI_STRING    CookedHeader;
    BOOL            fResult;

    // Build a cooked header in the form of
    // "name: value\r\n"
    fResult = CookedHeader.Printf("%s: %s\r\n", szName, szValue);

    if (!fResult) {
        goto Failed;
    }

    // Append the cooked header into the buffer
    fResult = _HeaderBuffer.Append( CookedHeader.QueryStr() );

    return fResult;
Failed:
    return FALSE;
}

DWORD SyncSendGenericServerError(EXTENSION_CONTROL_BLOCK *   pecb)
{
    ISAPI_REQUEST   Request( pecb );

    Request.SyncWriteCompleteResponse( "500 Server Error",
                                       "Content-Type: text/html\r\n\r\n",
                                       SERVER_ERROR );

    return HSE_STATUS_ERROR;
}


