/*****************************************************************************
 *
 * This file is part of Mapnik (c++ mapping toolkit)
 *
 * Copyright (C) 2011 Artem Pavlenko
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *****************************************************************************/

#ifndef MAPNIK_OCTREE_HPP
#define MAPNIK_OCTREE_HPP

// mapnik
//#include "global.h"
#include "palette.h"
//#include <mapnik/noncopyable.hpp>

// stl
#include <vector>
#include <deque>
#include <algorithm>

struct RGBPolicy
{
    const static unsigned MAX_LEVELS = 6;
    const static unsigned MIN_LEVELS = 2;
    inline static unsigned index_from_level(unsigned level, rgb const& c)
    {
        unsigned shift = 7 - level;
        return (((c.r >> shift) & 1) << 2)
            | (((c.g >> shift) & 1) << 1)
            | ((c.b >> shift) & 1);
    }
    inline static unsigned ss_index_from_level(unsigned level, rgb const& c)
    {
        unsigned shift = 7 - level;
        return (((c.r >> shift) & 1) << 2)
            | (((c.g >> shift) & 1) << 1)
            | ((c.b >> shift) & 1);
    }
};

template <typename T, typename InsertPolicy = RGBPolicy >
class octree //: private mapnik::noncopyable
{
    struct node
    {
        node()
            :  reds(0),
               greens(0),
               blues(0),
               count(0),
               count_cum(0),
               children_count(0),
               index(0),
			   is_leafb(false)
        {
            memset(&children_[0],0,sizeof(children_));
        }

        ~node()
        {

            for (unsigned i = 0;i < 8; ++i)
            {
                if (children_[i] != 0)
                {
                    delete children_[i];
                    children_[i]=0;
                }
            }
        }

        bool is_leaf() const
        {
            return count == 0;
        }
        node * children_[8];
		unsigned __int64 reds;
        unsigned __int64 greens;
        unsigned __int64 blues;
        unsigned count;
        double reduce_cost;
        unsigned count_cum;
        byte children_count;
        byte index;
		bool is_leafb;
    };
    struct node_cmp
    {
        bool operator() ( const node * lhs,const node* rhs) const
        {
            return lhs->reduce_cost < rhs->reduce_cost;
        }
    };
    struct node_cmp_by_count
    {
        bool operator() ( const int lhs,const int rhs) const
        {

            return lhs > rhs;
        }
    };


    std::deque<node*> reducible_[InsertPolicy::MAX_LEVELS];
    unsigned max_colors_;
    unsigned colors_;
    unsigned offset_;
    unsigned leaf_level_;

public:
    explicit octree(unsigned max_colors=256)
        : max_colors_(max_colors),
          colors_(0),
          offset_(0),
          leaf_level_(InsertPolicy::MAX_LEVELS),
          root_(new node())
    {}

    ~octree()
    {
        delete root_;
    }

    unsigned colors()
    {
        return colors_;
    }

    void setMaxColors(unsigned max_colors)
    {
        max_colors_ = max_colors;
    }

    void setOffset(unsigned offset)
    {
        offset_ = offset;
    }

    unsigned getOffset()
    {
        return offset_;
    }

    void insert(T const& data)
    {
        unsigned level = 0;
        node * cur_node = root_;
        while (true) //������ ��� ��������� ���� ������� � root � �������� �� ������ ����
        {
            cur_node->count_cum++; 
            cur_node->reds   += data.r; // ����� ������� � ������� ���� ����� ����� ����� ���� ������ �������� (��� ������� �� ���-�� ������ - ������� ����)
            cur_node->greens += data.g; // ����� ������� � ������� ���� ����� ����� ����� ���� ������ �������� (��� ������� �� ���-�� ������ - ������� ����)
            cur_node->blues  += data.b; // ����� ������� � ������� ���� ����� ����� ����� ���� ������ �������� (��� ������� �� ���-�� ������ - ������� ����)

            if ( cur_node->count > 0 || level == leaf_level_) // leaf_level_ ��� ������������� ����� InsertPolicy::MAX_LEVELS. ��� ��� ���������� cur_node->count = 0, �� ��� ������� ����� ����������� ������ ��� level == leaf_level_,
			// ���� ���� �� ��� ��������� � ������ ����������� ����
            {
                cur_node->count  += 1; // ���������� �� ����������� �������� count ��� ����, ������ ��� ���������� ������
				cur_node->is_leafb = true;
                if (cur_node->count == 1) ++colors_; // ����������� ���������� ������������ ������ �� 1.
                //if (colors_ >= max_colors_ - 1)
                //reduce();
                break; // ��������� ���������� �����
            }

            unsigned idx = InsertPolicy::index_from_level(level,data); //�� ����� � ������ ��������� ������ ��������� ����
            if (cur_node->children_[idx] == 0)
            {
                cur_node->children_count++;
                cur_node->children_[idx] = new node();
                if (level < leaf_level_-1) // ���� �� ��������� �������, �� ��������� ���� � ��������� reducible
                {
                    reducible_[level+1].push_back(cur_node->children_[idx]); // ��������� ��������� reducible ��� ������� ������
                }
            }
            cur_node = cur_node->children_[idx]; // ���� ������ ������ ������
            ++level;
        }
    }

    int quantize(unsigned val) const
    {
        unsigned level = 0;
        rgb c(val);
        node * cur_node = root_;
        while (cur_node)
        {
            if (cur_node->children_count == 0)
            {
                return cur_node->index + offset_;
            }
            unsigned idx = InsertPolicy::index_from_level(level,c);
            cur_node = cur_node->children_[idx];
            ++level;
        }
        return -1;
    }

    int ss_quantize(unsigned val) const
    {
        unsigned level = 0;
        rgb c(val);
        node * cur_node = root_;
		int min_delta_index = 0;		
        while (cur_node)
        {
            if (cur_node->children_count == 0)
            {
				if(cur_node->is_leafb)
				{
					return cur_node->index + offset_;
				}
				return 0;
            }

			//���������� ����� ��������� ���� ������
			if (min_delta_index < 0)
			{
				for (unsigned i = 0; i < 8 ; ++i) {
					if (cur_node->children_[i] != 0) {
						cur_node = cur_node->children_[i];
						break;
					}
				}
			}
			//���������� ����� ��������� ���� �����
			else if(min_delta_index > 0)
			{
				for (unsigned i = 7; i >= 0 ; --i) {
					if (cur_node->children_[i] != 0) {
						cur_node = cur_node->children_[i];
						break;
					}
				}
			}
			//���������� ����� ���� � �������� ����
			else
			{
				unsigned idx = InsertPolicy::index_from_level(level, c);
				if(cur_node->children_[idx] != 0)
				{
					cur_node = cur_node->children_[idx];
					++level;
				}
				else
				{
					//return -1;

					//���� ���������
					unsigned closest_index = 9;
					min_delta_index = 9;
					for (unsigned i = 0; i < 8 ; ++i)
					{
						if (cur_node->children_[i] != 0)
						{
							int dID = idx - i;
							if (std::abs(dID) < std::abs(min_delta_index))
							{
								min_delta_index = idx - i;
								closest_index = i;
							}
						}
					}
					if (min_delta_index == 9)
					{
						min_delta_index = 0;
					}
					cur_node = cur_node->children_[closest_index];
					++level;
				}
			}
        }
        return -1;
    }

    void create_palette(std::vector<rgb> & palette)
    {
        reduce();
        palette.reserve(colors_);
        create_palette(palette, root_);
    }

    void ss_create_palette(std::vector<rgb> & palette)
    {
        ss_reduce();
        palette.reserve(colors_);
        ss_create_palette(palette, root_);
    }

    void computeCost(node *r)
    {
        r->reduce_cost = 0;
        if (r->children_count==0)
        {
            return;
        }

        double mean_r = static_cast<double>(r->reds / r->count_cum);
        double mean_g = static_cast<double>(r->greens / r->count_cum);
        double mean_b = static_cast<double>(r->blues / r->count_cum);
        for (unsigned idx=0; idx < 8; ++idx)
        {
            if (r->children_[idx] != 0)
            {
                double dr,dg,db;
                computeCost(r->children_[idx]); //��� ���������� ������ ����� ������ �� �������
                dr = r->children_[idx]->reds   / r->children_[idx]->count_cum - mean_r;
                dg = r->children_[idx]->greens / r->children_[idx]->count_cum - mean_g;
                db = r->children_[idx]->blues  / r->children_[idx]->count_cum - mean_b;
                r->reduce_cost += r->children_[idx]->reduce_cost;
                r->reduce_cost += (dr*dr + dg*dg + db*db) * r->children_[idx]->count_cum; //(dr*dr + dg*dg + db*db) - ��� ���������� ����� ������� ������ ������������� ���� � ������� ������ ��������� ����, ����� ��� ���������� �� ���������� ��������� � �������� ����
				//��� ������ ���������� � ��� ������ ���������, ��� reduceCost ����� ������
            }
        }
    }

    void reduce() //���������� ��� ���������� �� ������
    {
	
        computeCost(root_); //������� ���������
        reducible_[0].push_back(root_);

        // sort reducible by reduce_cost
        for (unsigned i=0;i<InsertPolicy::MAX_LEVELS;++i)
        {
            std::sort(reducible_[i].begin(), reducible_[i].end(),node_cmp());
        }
        while (colors_ > max_colors_ && colors_ > 1)
        {
            while (leaf_level_ >0  && reducible_[leaf_level_-1].size() == 0) //���� ������� ��� �������� ��������� reducible �����
            {
                --leaf_level_;
            }

            if (leaf_level_ <= 0)
            {
                return;
            }

            // select best of all reducible:
            unsigned red_idx = leaf_level_-1; //�������� � �������������� ������
            unsigned bestv = static_cast<unsigned>((*reducible_[red_idx].begin())->reduce_cost); //���� reduce_cost ��� ������� ����� �������������� ������ �� ��������� reducible

			//����� ������ �������� �������� ����� ����� red_idx (������ ������), �� ������� ����� ����� ����������� reduce
            for(unsigned i=red_idx; i>=InsertPolicy::MIN_LEVELS; i--) // ���� ����� �����
            {
                if (!reducible_[i].empty())
                {
                    node *nd = *reducible_[i].begin();  
                    unsigned gch = 0;
                    for(unsigned idx=0; idx<8; idx++)
                    {
                        if (nd->children_[idx])
                            gch += nd->children_[idx]->children_count;
                    }
                    if (gch==0 && nd->reduce_cost < bestv) //���� ����� � ������� ��� ��������� � reduce_cost ������ �����������
                    {
                        bestv = static_cast<unsigned>(nd->reduce_cost);
                        red_idx = i; //����� ������ ������
                    }
                }
            }

            typename std::deque<node*>::iterator pos = reducible_[red_idx].begin();
            node * cur_node = *pos;
            unsigned num_children = 0;
			// ��������� �������� ��������� count �� �������� ����� � ������������, ���� �������� ����� �������������� ��� ���������� �������
			// ������� �������� ����
            for (unsigned idx=0; idx < 8; ++idx) 
            {
                if (cur_node->children_[idx] != 0)
                {
                    cur_node->children_count--;
                    ++num_children;
                    cur_node->count  += cur_node->children_[idx]->count; // ��������� �������� ��������� count �� ��������� ���� � ������������, ���� �������� ����� �������������� ��� ���������� �������
                    //todo: case of nonleaf children, if someday sorting by reduce_cost doesn't handle it
                    delete cur_node->children_[idx], cur_node->children_[idx]=0;
                }
            }

            reducible_[red_idx].erase(pos);
            if (num_children > 0 )
            {
                colors_ -= (num_children - 1);
            }
        }
    }


    void ss_reduce() //���� ������ 256 �������� ����� ������������ ������
    {
        computeCost(root_); //������� ���������
        reducible_[0].push_back(root_);

		//!!!
		unsigned min_count_edge = ss_get_min_count_edge(root_);
		
		//std::sort(reducible_[InsertPolicy::MAX_LEVELS - 1].begin(), reducible_[InsertPolicy::MAX_LEVELS - 1].end(), node_cmp_by_count());

		//1. ����� ���������� �� ���������� ��������� �������, ������� ����������� ����� ������������� ��� ������ 256 ������.
		//unsigned last_level_items_count = reducible_[InsertPolicy::MAX_LEVELS - 1].size();
		//unsigned min_count_edge;
		unsigned deleted_children = 0;
		unsigned total_children = 0;
		if(min_count_edge > 0)
		{
			//node* lll = reducible_[InsertPolicy::MAX_LEVELS - 1][255];
			//min_count_edge = lll->count;

			//2. ������� ������ � ����������� ����� ������������ �����
			unsigned red_idx = leaf_level_-1; //�������� � �������������� ������
			while (!reducible_[red_idx].empty())
			{
				typename std::deque<node*>::iterator pos = reducible_[red_idx].begin();
				node * cur_node = *pos;
				// ��������� �������� ��������� count �� �������� ����� � ������������, ���� �������� ����� �������������� ��� ���������� �������
				// ������� �������� ����
				for (unsigned idx = 0; idx < 8; ++idx) 
				{
					/*
					unsigned max_count = 0;
					unsigned max_count_idx = 0;
					if (cur_node->children_[idx] != 0)
					{
						if(cur_node->children_[idx]->count > max_count)
						{
							max_count = cur_node->children_[idx]->count;
							max_count_idx = idx;
						}
					}
					*/
					if (cur_node->children_[idx] != 0 && cur_node->children_[idx]->count < min_count_edge + 1)
					{
						cur_node->children_count--;
						delete cur_node->children_[idx];
						cur_node->children_[idx] = 0;
						deleted_children++;
					}
					total_children++;
				}
				reducible_[red_idx].erase(pos);
			}
		

			//����� ������ �������� �������� ����� ����� red_idx (������ ������), �� ������� ����� ����� ����������� reduce
			
			for(unsigned i=leaf_level_- 2; i>=InsertPolicy::MIN_LEVELS; i--) // ���� ����� �����
			{
				if (!reducible_[i].empty())
				{
					node *nd = *reducible_[i].begin();  
					for(unsigned idx=0; idx<8; idx++)
					{
						if (nd->children_[idx] && nd->children_[idx]->children_count == 0)
						{
							nd->children_count--;
							delete nd->children_[idx];
							nd->children_[idx] = 0;
						}
					}
				}
			}
			
		}

		//!!!
    }

    void create_palette(std::vector<rgb> & palette, node * itr) const
    {
        if (itr->count != 0)
        {
            unsigned count = itr->count;
            palette.push_back(rgb(byte(itr->reds/float(count)),
                                  byte(itr->greens/float(count)),
                                  byte(itr->blues/float(count))));
            itr->index = palette.size() - 1;
        }
        for (unsigned i = 0; i < 8 ; ++i)
        {
            if (itr->children_[i] != 0)
            {
                create_palette(palette, itr->children_[i]);
            }
        }
    }

    void ss_create_palette(std::vector<rgb> & palette, node * itr) const
    {
		if(itr->children_count == 0 && itr->is_leafb)
		//else if(itr->is_leafb && itr->reds != 0 && itr->greens != 0 && itr->blues != 0)
		{
			unsigned count = itr->count;
			palette.push_back(rgb(byte(itr->reds/float(count)),
									byte(itr->greens/float(count)),
									byte(itr->blues/float(count))));
			itr->index = palette.size() - 1;
			return;
		}

		//����� ������ ������, ��������� ������ ������ � �������
        for (unsigned i = 0; i < 8 ; ++i)
        {
            if (itr->children_[i] != 0)
            {
                ss_create_palette(palette, itr->children_[i]);
            }
        }
    }

	void ss_min_count_edge(std::vector<unsigned>* counts, node * itr)
	{
		if(itr->children_count == 0 && itr->is_leafb)
		{
			counts->push_back(itr->count);
			return;
		}

		//����� ������ ������, ��������� ������ ������ � �������
        for (unsigned i = 0; i < 8 ; ++i)
        {
            if (itr->children_[i] != 0)
            {
                ss_min_count_edge(counts, itr->children_[i]);
            }
        }
	}

	unsigned ss_get_min_count_edge(node * itr)
	{
		unsigned min_count_edge = 0;	
		std::vector<unsigned> counts;
		ss_min_count_edge(&counts, itr);
		std::sort(counts.begin(), counts.end(), node_cmp_by_count());

		if(counts.size() > 256)
		{
			min_count_edge = counts[255];
		}

		return min_count_edge;
	}

	void ss_clear_tree(node * itr)
	{
		//����� ������ ������, ��������� ������ ������ � �������
        for (unsigned i = 0; i < 8 ; ++i)
        {
            if (itr->children_[i] != 0)
            {
				if(itr->children_[i]->children_count == 0)
				{
					itr->children_count--;
					delete itr->children_[i];
					itr->children_[i] = 0;
				}
				else
				{
					ss_clear_tree(itr->children_[i]);
				}
            }
        }
	}

private:
    node * root_;
};


#endif // MAPNIK_OCTREE_HPP
