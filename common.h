#pragma once

#ifndef COMMON_H
#define COMMON_H

#include <windows.h>
#include "httpext.h"
#include <process.h>
#include <ATLBASE.H>
#include "atl7extr.h"

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "string.h"

#include <math.h>
#include <algorithm>
#include <vector>
#include <map>


#include <boost/tokenizer.hpp>
#include <boost/token_functions.hpp>
/*
#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/format.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/filesystem.hpp>
*/

#include <png.h>
#include <gdiplus.h>
#pragma comment(lib, "gdiplus.lib")

//#include "image_data.h"
//#include "BitStream.h"
//#include "palette.h"
//#include "octree.h"

// typedefs
typedef boost::tokenizer<boost::char_separator<char>> tokenizer;
typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned int uint;
typedef unsigned long ulong;
typedef unsigned char BYTE;
typedef unsigned long COLORREF;
typedef COLORREF *PCOLORREF;


//�������
#define SAFE_DELETE(p) \
	if( NULL != p ) \
{ \
	delete p; \
	p = NULL; \
}


void ExtensionThreadProc(LPVOID);

inline bool ReadCodecsList(bool Encoders, GUID *pGUID)
{
	bool fFound = false;

	using namespace Gdiplus;
	UINT  num, size;   
	if(Encoders) 
		GetImageEncodersSize(&num, &size);
	else
		GetImageDecodersSize(&num, &size);

	// ������ ������ - � ������!
	ImageCodecInfo* pArray = (ImageCodecInfo*)(malloc(size));
	if(Encoders) 
	GetImageEncoders(num, size, pArray);
	else
	GetImageDecoders(num, size, pArray);

	// ��������� map
	for(UINT j = 0; j < num; ++j)
	{
		if( wcscmp(pArray[j].MimeType, L"image/png") == 0 )
		{
			*pGUID = pArray[j].Clsid;
			fFound = true;
			break;
		}    
	}

	free(pArray);
	return fFound;
}

inline void QuadKeyToTileXY(char* quadKey, int* tileX, int* tileY, int* levelOfDetail)
{
    *tileX = 0;
	*tileY = 0;
	*levelOfDetail = strlen(quadKey);
    for (int i = *levelOfDetail; i > 0; i--)
    {
        int mask = 1 << (i - 1);
        switch (quadKey[*levelOfDetail - i])
        {
            case '0':
                break;

            case '1':
                *tileX |= mask;
                break;

            case '2':
                *tileY |= mask;
                break;

            case '3':
                *tileX |= mask;
                *tileY |= mask;
                break;

            default:
                break; //throw new ArgumentException("Invalid QuadKey digit sequence.");
        }
    }
}

inline void QuadKeyToTileXY(const char* quadKey, int* tileX, int* tileY, int* levelOfDetail)
{
    *tileX = 0;
	*tileY = 0;
	*levelOfDetail = strlen(quadKey);
    for (int i = *levelOfDetail; i > 0; i--)
    {
        int mask = 1 << (i - 1);
        switch (quadKey[*levelOfDetail - i])
        {
            case '0':
                break;

            case '1':
                *tileX |= mask;
                break;

            case '2':
                *tileY |= mask;
                break;

            case '3':
                *tileX |= mask;
                *tileY |= mask;
                break;

            default:
                break; //throw new ArgumentException("Invalid QuadKey digit sequence.");
        }
    }
}

#endif // COMMON_H