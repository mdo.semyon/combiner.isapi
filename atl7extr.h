#if _MSC_VER < 1300

#ifndef _ATL7_EXTR_INCLUDED
#define _ATL7_EXTR_INCLUDED

template< class TLock >
class CComCritSecLock
{
public:
	CComCritSecLock( TLock& cs, bool bInitialLock = true );
	~CComCritSecLock() throw();

	HRESULT Lock() throw();
	void Unlock() throw();

// Implementation
private:
	TLock& m_cs;
	bool m_bLocked;

// Private to avoid accidental use
	CComCritSecLock( const CComCritSecLock& );
	CComCritSecLock& operator=( const CComCritSecLock& );
};

template< class TLock >
inline CComCritSecLock< TLock >::CComCritSecLock( TLock& cs, bool bInitialLock ) :
	m_cs( cs ),
	m_bLocked( false )
{
	if( bInitialLock )
	{
		Lock();
	}
}

template< class TLock >
inline CComCritSecLock< TLock >::~CComCritSecLock() throw()
{
	if( m_bLocked )
	{
		Unlock();
	}
}

template< class TLock >
inline HRESULT CComCritSecLock< TLock >::Lock() throw()
{
	ATLASSERT( !m_bLocked );
	m_cs.Lock();
	m_bLocked = true;

	return( S_OK );
}

template< class TLock >
inline void CComCritSecLock< TLock >::Unlock() throw()
{
	ATLASSERT( m_bLocked );
	m_cs.Unlock();
	m_bLocked = false;
}

#endif //_ATL7_EXTR_INCLUDED

#endif //_MSC_VER < 1300
