#ifndef CONFIG_H
#define CONFIG_H

#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
/*
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
*/
#include <boost/filesystem.hpp>

struct Layer
{
	std::string		sysName;
	std::string		path;
	std::string		gso; 
	unsigned		minZoom;
	unsigned		maxZoom;
	unsigned		maxAvaluableZoom;
};

class Configuration
{
	public:
		bool loaded;
		bool use8bppIndexedConverter;
		std::string blankTile;
		std::string generatedRastrTilesPath;
		std::vector<Layer>* Layers;

	public:
		Configuration()
		{
			this->Layers = new std::vector<Layer>();
		}
		~Configuration()
		{
		}
		void readConfig(std::string dirName, std::string fileName)
		{
			//boost::filesystem::path abs_path = boost::filesystem::complete(this->m_fileName);
			//std::string abs_path_str = abs_path.string();
			//std::ifstream is(abs_path_str, std::ifstream::binary);
			std::string ddd = dirName + "\\" + fileName;
			std::ifstream is(ddd, std::ifstream::binary);

			if(is)
			{
				// populate tree structure pt
				using boost::property_tree::ptree;
				ptree pt;
				read_xml(is, pt);
 
				// traverse pt
				this->use8bppIndexedConverter = pt.get("Combiner.<xmlattr>.use8bppIndexedConverter", false);
				this->blankTile = pt.get("Combiner.<xmlattr>.blankTile", dirName + "\\blankTile.png");
				this->generatedRastrTilesPath = pt.get("Combiner.<xmlattr>.generatedRastrTilesPath", "");
				BOOST_FOREACH( ptree::value_type const& v, pt.get_child("Combiner") ) {
			
					if( v.first == "add" ) {
						Layer l;
						l.sysName = v.second.get<std::string>("sysName");
						l.path = v.second.get<std::string>("path");
						l.minZoom = v.second.get<unsigned>("minZoom");
						l.maxZoom = v.second.get<unsigned>("maxZoom");
						l.maxAvaluableZoom = v.second.get<unsigned>("maxAvaluableZoom");
						this->Layers->push_back(l);
					}
				}
			}
		}
	private:
		//std::string m_fileName;

};


#endif