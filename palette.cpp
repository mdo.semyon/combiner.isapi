#include "palette.h"

// stl
#include <sstream>
#include <iomanip>


rgb::rgb(rgba const& c)
    : r(c.r), g(c.g), b(c.b) {}

// ordering by mean(a,r,g,b), a, r, g, b
bool rgba::mean_sort_cmp::operator() (const rgba& x, const rgba& y) const
{
    int t1 = (int)x.a + x.r + x.g + x.b;
    int t2 = (int)y.a + y.r + y.g + y.b;
    if (t1 != t2) return t1 < t2;

    // https://github.com/mapnik/mapnik/issues/1087
    if (x.a != y.a) return x.a < y.a;
    if (x.r != y.r) return x.r < y.r;
    if (x.g != y.g) return x.g < y.g;
    return x.b < y.b;
}

rgba_palette::rgba_palette(std::string const& pal, palette_type type)
    : colors_(0)
{
	#ifdef USE_DENSE_HASH_MAP
		color_hashmap_.set_empty_key(0);
	#endif
    //parse(pal, type);
}

rgba_palette::rgba_palette()
    : colors_(0)
{
	#ifdef USE_DENSE_HASH_MAP
		color_hashmap_.set_empty_key(0);
	#endif
}

const std::vector<rgb>& rgba_palette::palette() const
{
    return rgb_pal_;
}

const std::vector<unsigned>& rgba_palette::alphaTable() const
{
    return alpha_pal_;
}

bool rgba_palette::valid() const
{
    return colors_ > 0;
}