//
//  Values are 32 bit values laid out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//


//
// Define the severity codes
//


//
// MessageId: MSG_ISAPI_INITIALIZED
//
// MessageText:
//
// ISAPI Extension Initialized.
//
#define MSG_ISAPI_INITIALIZED            0x40000001L

//
// MessageId: MSG_ISAPI_UNINITIALIZED
//
// MessageText:
//
// ISAPI Extension Uninitialized.
//
#define MSG_ISAPI_UNINITIALIZED          0x40000002L

//
// MessageId: MSG_ISAPI_INITIALIZATION_FAILED
//
// MessageText:
//
// Initialization failed. Please check configuration.
//
#define MSG_ISAPI_INITIALIZATION_FAILED  0xC0000201L

//
// MessageId: MSG_ISAPI_QUEUE_TIMEOUT
//
// MessageText:
//
// Queue timeout expired.
//
#define MSG_ISAPI_QUEUE_TIMEOUT          0xC0000202L

//
// MessageId: MSG_ISAPI_THREAD_FAILED
//
// MessageText:
//
// Thread creation failed.
//
#define MSG_ISAPI_THREAD_FAILED          0xC0000203L

//
// MessageId: MSG_ISAPI_LIBPNG_ERROR
//
// MessageText:
//
// Error occured during libpng operation
//
#define MSG_ISAPI_LIBPNG_ERROR           0xC0000204L

//
// MessageId: MSG_ISAPI_LIBPNG_WARNING
//
// MessageText:
//
// libpng executed with warning
//
#define MSG_ISAPI_LIBPNG_WARNING         0x80000205L

