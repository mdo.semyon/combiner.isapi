#ifndef APPROXIMATOR_H
#define APPROXIMATOR_H

//#include "common.h"
#include <boost/filesystem.hpp>
#include "string.h"
#include <gdiplus.h>
#pragma comment(lib, "gdiplus.lib")

#include "Utils.h"

class Approximator
{
	public:
		Approximator(std::string blankTile)
		{
			this->m_blankTile = blankTile;
		}
		~Approximator()
		{
		}

		std::string GetTilePath(std::string layerPath, int tileX, int tileY, int tileZ)
		{
			return this->string_format(layerPath + "\\%i\\%i\\%i.png", tileZ, tileX, tileY);
		}

		bool GetApproximatedTile(Gdiplus::Bitmap* outputImage, char* tileNumber, std::string layerPath, int lastAvailableZoom)
		{
			bool m_Processed = false;
			std::string path;
			wchar_t* wc;
			int level = strlen(tileNumber);
			int originalTileX;
			int originalTileY;
			int originalTileZ;

			char originalTileQuadkey[17];
			memcpy(originalTileQuadkey, tileNumber, lastAvailableZoom);
			originalTileQuadkey[lastAvailableZoom] = '\0';

			Gdiplus::Bitmap* sourceImage = new Gdiplus::Bitmap(512, 512, PixelFormat32bppARGB);

			if (level > lastAvailableZoom)
			{
				int tileLength = 256;
				int minX = 0, minY = 0;

				for (int i = lastAvailableZoom; i < level; i++)
				{
					tileLength /= 2;

					switch (tileNumber[i])
					{
						case '0':
							break;
						case '2':
							minY += tileLength;
							break;
						case '1':
							minX += tileLength;
							break;
						case '3':
							minX += tileLength;
							minY += tileLength;
							break;
					}
				}

				QuadKeyToTileXY(originalTileQuadkey, &originalTileX, &originalTileY, &originalTileZ);
				path = this->GetTilePath(layerPath, originalTileX, originalTileY, originalTileZ);
				if (boost::filesystem::exists(path))
				{
					wc = utils::charToWChar((char*)path.c_str());
					Gdiplus::Bitmap* tile = new Gdiplus::Bitmap(wc, false);
					//delete path;
					delete[] wc;
					if(tile)
					{
						if (tile->GetLastStatus() == Gdiplus::Ok)
						{
							Gdiplus::Graphics* graphics = Gdiplus::Graphics::FromImage((Gdiplus::Image*)outputImage);
							graphics->SetInterpolationMode(Gdiplus::InterpolationModeBilinear);

							Gdiplus::Graphics* sourceGraphics = Gdiplus::Graphics::FromImage((Gdiplus::Image*)sourceImage);
			
							Gdiplus::RectF* rect = new Gdiplus::RectF(0, 0, 256, 256);
							sourceGraphics->DrawImage(tile, *rect, 0, 0, 256, 256, Gdiplus::Unit::UnitPixel);

							//bottom
							if (minX + tileLength == 256)
							{
								std::string pathx = this->GetTilePath(layerPath, originalTileX + 1, originalTileY, originalTileZ);
								if (boost::filesystem::exists(pathx))
								{
									wc = utils::charToWChar(pathx.c_str());
									Gdiplus::Bitmap* xOffsetTile = new Gdiplus::Bitmap(wc, false);
									if(xOffsetTile)
									{
										if (xOffsetTile->GetLastStatus() == Gdiplus::Ok)
										{
											Gdiplus::RectF* rectB = new Gdiplus::RectF(256, 0, 8, 256);
											sourceGraphics->DrawImage(xOffsetTile, *rectB, 0, 0, 8, 256, Gdiplus::Unit::UnitPixel);
											SAFE_DELETE(rectB);
										}
										SAFE_DELETE(xOffsetTile);
									}
									//delete path;
									delete[] wc;
								}
							}
							//right
							if (minY + tileLength == 256)
							{
								std::string pathy = this->GetTilePath(layerPath, originalTileX, originalTileY + 1, originalTileZ);
								if (boost::filesystem::exists(pathy))
								{
									wc = utils::charToWChar(pathy.c_str());
									Gdiplus::Bitmap* yOffsetTile = new Gdiplus::Bitmap(wc, false);
									if(yOffsetTile)
									{
										if (yOffsetTile->GetLastStatus() == Gdiplus::Ok)
										{
											Gdiplus::RectF* rectR = new Gdiplus::RectF(0, 256, 256, 8);
											sourceGraphics->DrawImage(yOffsetTile, *rectR, 0, 0, 256, 8, Gdiplus::Unit::UnitPixel);
											SAFE_DELETE(rectR);
										}
										SAFE_DELETE(yOffsetTile);
									}
									//delete path;
									delete[] wc;
								}
							}

							//bottom, right
							if (minX + tileLength == 256 && minY + tileLength == 256)
							{
								std::string pathxy = this->GetTilePath(layerPath, originalTileX + 1, originalTileY + 1, originalTileZ);
								if (boost::filesystem::exists(pathxy))
								{
									wc = utils::charToWChar(pathxy.c_str());
									Gdiplus::Bitmap* xyOffsetTile = new Gdiplus::Bitmap(wc, false);
									if(xyOffsetTile)
									{
										if (xyOffsetTile->GetLastStatus() == Gdiplus::Ok)
										{
											Gdiplus::RectF* rectBR = new Gdiplus::RectF(256, 256, 8 ,8);
											sourceGraphics->DrawImage(xyOffsetTile, *rectBR, 0, 0, 8, 8, Gdiplus::Unit::UnitPixel);
											SAFE_DELETE(rectBR);
										}
										SAFE_DELETE(xyOffsetTile);
									}
									//delete path;
									delete[] wc;
								}
							}

							graphics->DrawImage(sourceImage, *rect, minX, minY, tileLength, tileLength, Gdiplus::Unit::UnitPixel);

							SAFE_DELETE(rect);
							SAFE_DELETE(sourceGraphics);
							SAFE_DELETE(sourceImage);
							SAFE_DELETE(graphics);
							m_Processed = true;
						}
						SAFE_DELETE(tile);
					}
				}
				
				if(!m_Processed)
				{
					if (boost::filesystem::exists(this->m_blankTile))
					{
						wc = utils::charToWChar(this->m_blankTile.c_str());
						outputImage = new Gdiplus::Bitmap(wc, false);
						if(outputImage)
						{
							if (outputImage->GetLastStatus() == Gdiplus::Ok)
							{
								m_Processed = true;
							}
						}
						delete[] wc;
					}
				}
			}

			return m_Processed;
		}

	private:
		std::string string_format(const std::string fmt, ...) {
			int size = 100;
			std::string str;
			va_list ap;
			while (1) {
				str.resize(size);
				va_start(ap, fmt);
				int n = vsnprintf((char *)str.c_str(), size, fmt.c_str(), ap);
				va_end(ap);
				if (n > -1 && n < size) {
					str.resize(n);
					return str;
				}
				if (n > -1)
					size = n + 1;
				else
					size *= 2;
			}

			return str;
		}
	private:
		std::string m_blankTile;
};

#endif