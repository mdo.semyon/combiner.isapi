//����������� ��� libpng
#define PNG_16BIT_SUPPORTED
#define PNG_ALIGNED_MEMORY_SUPPORTED
/*#undef PNG_ARM_NEON_API_SUPPORTED*/
/*#undef PNG_ARM_NEON_CHECK_SUPPORTED*/
#define PNG_BENIGN_ERRORS_SUPPORTED
#define PNG_BENIGN_READ_ERRORS_SUPPORTED
/*#undef PNG_BENIGN_WRITE_ERRORS_SUPPORTED*/
#define PNG_BUILD_GRAYSCALE_PALETTE_SUPPORTED
#define PNG_CHECK_FOR_INVALID_INDEX_SUPPORTED
#define PNG_COLORSPACE_SUPPORTED
#define PNG_CONSOLE_IO_SUPPORTED
#define PNG_CONVERT_tIME_SUPPORTED
#define PNG_EASY_ACCESS_SUPPORTED
/*#undef PNG_ERROR_NUMBERS_SUPPORTED*/
#define PNG_ERROR_TEXT_SUPPORTED
#define PNG_FIXED_POINT_SUPPORTED
#define PNG_FLOATING_ARITHMETIC_SUPPORTED
#define PNG_FLOATING_POINT_SUPPORTED
#define PNG_FORMAT_AFIRST_SUPPORTED
#define PNG_FORMAT_BGR_SUPPORTED
#define PNG_GAMMA_SUPPORTED
#define PNG_GET_PALETTE_MAX_SUPPORTED
#define PNG_HANDLE_AS_UNKNOWN_SUPPORTED
#define PNG_INCH_CONVERSIONS_SUPPORTED
#define PNG_INFO_IMAGE_SUPPORTED
#define PNG_IO_STATE_SUPPORTED
#define PNG_MNG_FEATURES_SUPPORTED
#define PNG_POINTER_INDEXING_SUPPORTED
#define PNG_PROGRESSIVE_READ_SUPPORTED
#define PNG_READ_16BIT_SUPPORTED
#define PNG_READ_ALPHA_MODE_SUPPORTED
#define PNG_READ_ANCILLARY_CHUNKS_SUPPORTED
#define PNG_READ_BACKGROUND_SUPPORTED
#define PNG_READ_BGR_SUPPORTED
#define PNG_READ_CHECK_FOR_INVALID_INDEX_SUPPORTED
#define PNG_READ_COMPOSITE_NODIV_SUPPORTED
#define PNG_READ_COMPRESSED_TEXT_SUPPORTED
#define PNG_READ_EXPAND_16_SUPPORTED
#define PNG_READ_EXPAND_SUPPORTED
#define PNG_READ_FILLER_SUPPORTED
#define PNG_READ_GAMMA_SUPPORTED
#define PNG_READ_GET_PALETTE_MAX_SUPPORTED
#define PNG_READ_GRAY_TO_RGB_SUPPORTED
#define PNG_READ_INTERLACING_SUPPORTED
#define PNG_READ_INT_FUNCTIONS_SUPPORTED
#define PNG_READ_INVERT_ALPHA_SUPPORTED
#define PNG_READ_INVERT_SUPPORTED
#define PNG_READ_OPT_PLTE_SUPPORTED
#define PNG_READ_PACKSWAP_SUPPORTED
#define PNG_READ_PACK_SUPPORTED
#define PNG_READ_QUANTIZE_SUPPORTED
#define PNG_READ_RGB_TO_GRAY_SUPPORTED
#define PNG_READ_SCALE_16_TO_8_SUPPORTED
#define PNG_READ_SHIFT_SUPPORTED
#define PNG_READ_STRIP_16_TO_8_SUPPORTED
#define PNG_READ_STRIP_ALPHA_SUPPORTED
#define PNG_READ_SUPPORTED
#define PNG_READ_SWAP_ALPHA_SUPPORTED
#define PNG_READ_SWAP_SUPPORTED
#define PNG_READ_TEXT_SUPPORTED
#define PNG_READ_TRANSFORMS_SUPPORTED
#define PNG_READ_UNKNOWN_CHUNKS_SUPPORTED
#define PNG_READ_USER_CHUNKS_SUPPORTED
#define PNG_READ_USER_TRANSFORM_SUPPORTED
#define PNG_READ_bKGD_SUPPORTED
#define PNG_READ_cHRM_SUPPORTED
#define PNG_READ_gAMA_SUPPORTED
#define PNG_READ_hIST_SUPPORTED
#define PNG_READ_iCCP_SUPPORTED
#define PNG_READ_iTXt_SUPPORTED
#define PNG_READ_oFFs_SUPPORTED
#define PNG_READ_pCAL_SUPPORTED
#define PNG_READ_pHYs_SUPPORTED
#define PNG_READ_sBIT_SUPPORTED
#define PNG_READ_sCAL_SUPPORTED
#define PNG_READ_sPLT_SUPPORTED
#define PNG_READ_sRGB_SUPPORTED
#define PNG_READ_tEXt_SUPPORTED
#define PNG_READ_tIME_SUPPORTED
#define PNG_READ_tRNS_SUPPORTED
#define PNG_READ_zTXt_SUPPORTED
/*#undef PNG_SAFE_LIMITS_SUPPORTED*/
#define PNG_SAVE_INT_32_SUPPORTED
#define PNG_SAVE_UNKNOWN_CHUNKS_SUPPORTED
#define PNG_SEQUENTIAL_READ_SUPPORTED
#define PNG_SETJMP_SUPPORTED
#define PNG_SET_CHUNK_CACHE_LIMIT_SUPPORTED
#define PNG_SET_CHUNK_MALLOC_LIMIT_SUPPORTED
#define PNG_SET_OPTION_SUPPORTED
#define PNG_SET_UNKNOWN_CHUNKS_SUPPORTED
#define PNG_SET_USER_LIMITS_SUPPORTED
#define PNG_SIMPLIFIED_READ_AFIRST_SUPPORTED
#define PNG_SIMPLIFIED_READ_BGR_SUPPORTED
#define PNG_SIMPLIFIED_READ_SUPPORTED
#define PNG_SIMPLIFIED_WRITE_AFIRST_SUPPORTED
#define PNG_SIMPLIFIED_WRITE_BGR_SUPPORTED
#define PNG_SIMPLIFIED_WRITE_SUPPORTED
#define PNG_STDIO_SUPPORTED
#define PNG_STORE_UNKNOWN_CHUNKS_SUPPORTED
#define PNG_TEXT_SUPPORTED
#define PNG_TIME_RFC1123_SUPPORTED
#define PNG_UNKNOWN_CHUNKS_SUPPORTED
#define PNG_USER_CHUNKS_SUPPORTED
#define PNG_USER_LIMITS_SUPPORTED
#define PNG_USER_MEM_SUPPORTED
#define PNG_USER_TRANSFORM_INFO_SUPPORTED
#define PNG_USER_TRANSFORM_PTR_SUPPORTED
#define PNG_WARNINGS_SUPPORTED
#define PNG_WRITE_16BIT_SUPPORTED
#define PNG_WRITE_ANCILLARY_CHUNKS_SUPPORTED
#define PNG_WRITE_BGR_SUPPORTED
#define PNG_WRITE_CHECK_FOR_INVALID_INDEX_SUPPORTED
#define PNG_WRITE_COMPRESSED_TEXT_SUPPORTED
#define PNG_WRITE_CUSTOMIZE_ZTXT_COMPRESSION_SUPPORTED
#define PNG_WRITE_FILLER_SUPPORTED
#define PNG_WRITE_FILTER_SUPPORTED
#define PNG_WRITE_FLUSH_SUPPORTED
#define PNG_WRITE_GET_PALETTE_MAX_SUPPORTED
#define PNG_WRITE_INTERLACING_SUPPORTED
#define PNG_WRITE_INT_FUNCTIONS_SUPPORTED
#define PNG_WRITE_INVERT_ALPHA_SUPPORTED
#define PNG_WRITE_INVERT_SUPPORTED
#define PNG_WRITE_OPTIMIZE_CMF_SUPPORTED
#define PNG_WRITE_PACKSWAP_SUPPORTED
#define PNG_WRITE_PACK_SUPPORTED
#define PNG_WRITE_SHIFT_SUPPORTED
#define PNG_WRITE_SUPPORTED
#define PNG_WRITE_SWAP_ALPHA_SUPPORTED
#define PNG_WRITE_SWAP_SUPPORTED
#define PNG_WRITE_TEXT_SUPPORTED
#define PNG_WRITE_TRANSFORMS_SUPPORTED
#define PNG_WRITE_UNKNOWN_CHUNKS_SUPPORTED
#define PNG_WRITE_USER_TRANSFORM_SUPPORTED
#define PNG_WRITE_WEIGHTED_FILTER_SUPPORTED
#define PNG_WRITE_bKGD_SUPPORTED
#define PNG_WRITE_cHRM_SUPPORTED
#define PNG_WRITE_gAMA_SUPPORTED
#define PNG_WRITE_hIST_SUPPORTED
#define PNG_WRITE_iCCP_SUPPORTED
#define PNG_WRITE_iTXt_SUPPORTED
#define PNG_WRITE_oFFs_SUPPORTED
#define PNG_WRITE_pCAL_SUPPORTED
#define PNG_WRITE_pHYs_SUPPORTED
#define PNG_WRITE_sBIT_SUPPORTED
#define PNG_WRITE_sCAL_SUPPORTED
#define PNG_WRITE_sPLT_SUPPORTED
#define PNG_WRITE_sRGB_SUPPORTED
#define PNG_WRITE_tEXt_SUPPORTED
#define PNG_WRITE_tIME_SUPPORTED
#define PNG_WRITE_tRNS_SUPPORTED
#define PNG_WRITE_zTXt_SUPPORTED
#define PNG_bKGD_SUPPORTED
#define PNG_cHRM_SUPPORTED
#define PNG_gAMA_SUPPORTED
#define PNG_hIST_SUPPORTED
#define PNG_iCCP_SUPPORTED
#define PNG_iTXt_SUPPORTED
#define PNG_oFFs_SUPPORTED
#define PNG_pCAL_SUPPORTED
#define PNG_pHYs_SUPPORTED
#define PNG_sBIT_SUPPORTED
#define PNG_sCAL_SUPPORTED
#define PNG_sPLT_SUPPORTED
#define PNG_sRGB_SUPPORTED
#define PNG_tEXt_SUPPORTED
#define PNG_tIME_SUPPORTED
#define PNG_tRNS_SUPPORTED
#define PNG_zTXt_SUPPORTED
/* end of options */
/* settings */
#define PNG_API_RULE 0
#define PNG_CALLOC_SUPPORTED
#define PNG_COST_SHIFT 3
#define PNG_DEFAULT_READ_MACROS 1
#define PNG_GAMMA_THRESHOLD_FIXED 5000
#define PNG_IDAT_READ_SIZE PNG_ZBUF_SIZE
#define PNG_INFLATE_BUF_SIZE 1024
#define PNG_MAX_GAMMA_8 11
#define PNG_QUANTIZE_BLUE_BITS 5
#define PNG_QUANTIZE_GREEN_BITS 5
#define PNG_QUANTIZE_RED_BITS 5
#define PNG_TEXT_Z_DEFAULT_COMPRESSION (-1)
#define PNG_TEXT_Z_DEFAULT_STRATEGY 0
#define PNG_WEIGHT_SHIFT 8
#define PNG_ZBUF_SIZE 8192
#define PNG_ZLIB_VERNUM 0 /* unknown */
#define PNG_Z_DEFAULT_COMPRESSION (-1)
#define PNG_Z_DEFAULT_NOFILTER_STRATEGY 0
#define PNG_Z_DEFAULT_STRATEGY 1
#define PNG_sCAL_PRECISION 5
#define PNG_sRGB_PROFILE_CHECKS 2

#define Z_DEFAULT_COMPRESSION -1
#define Z_DEFAULT_STRATEGY 0
#define PNG_BYTES_TO_CHECK 4
#define MAX_OCTREE_LEVELS 4

#define U2BLUE(x) ((x)&0xff)
#define U2GREEN(x) (((x)>>8)&0xff)
#define U2RED(x) (((x)>>16)&0xff)
#define U2ALPHA(x) (((x)>>24)&0xff)

#ifndef LIBPNGEXT_H
#define LIBPNGEXT_H

#include <png.h>
#include "image_data.h"
#include "BitStream.h"
#include "palette.h"
#include "octree.h"

void png_user_error_fn(png_structp png_ptr, png_const_charp error_msg)
{
	//BOOST_LOG_SEV(lg, boost::log::trivial::error) << "Libpng error";
}
void png_user_warning_fn(png_structp png_ptr, png_const_charp warning_msg)
{
	//BOOST_LOG_SEV(lg, boost::log::trivial::warning) << "Libpng warning";
}

class LibpngExt
{
	struct node_cmp_by_count
    {
        bool operator() ( const rgbn lhs,const rgbn rhs) const
        {

			return lhs.n > rhs.n;
        }
    };
	public:
		LibpngExt(void* write_fn, void* flush_fn)
		{
			this->write_data_fn = (png_rw_ptr)write_fn;
			this->output_flush_fn = (png_flush_ptr)flush_fn;
		}
		~LibpngExt()
		{
		}

		void readData(void* Scan0, image_data_32 &image)
		{
			memcpy(image.getRow(0), Scan0, 256 * 256 * 4);
		}
		void readDataT(void* Scan0, image_data_32 &image)
		{
			image_data_32 in(256, 256);
			memcpy(in.getRow(0), Scan0, 256 * 256 * 4);
			
			png_byte* row_in = (png_bytep)in.getRow(0);
			png_byte* row_out = (png_bytep)image.getRow(0);
			for (unsigned int i = 0; i < 256 * 256; i++)
			{
				unsigned r = *(row_in + i * 4);
				unsigned g = *(row_in + i * 4 + 1);
				unsigned b = *(row_in + i * 4 + 2);
				unsigned a = *(row_in + i * 4 + 3);

				*(row_out + i * 4) = b;
				*(row_out + i * 4 + 1) = g;
				*(row_out + i * 4 + 2) = r;
				*(row_out + i * 4 + 3) = a;
			}
			//delete &in;
		}
		void reduce_8(image_data_32 const& in,
					  image_data_8 & out,
					  octree<rgb> &tree,
					  std::vector<unsigned> & alpha)
		{
			unsigned width = in.width();
			unsigned height = in.height();

			std::vector<unsigned> alphaCount(alpha.size());
			for(unsigned i=0; i<alpha.size(); i++)
			{
				alpha[i] = 0;
				alphaCount[i] = 0;
			}
			for (unsigned y = 0; y < height; ++y)
			{
				image_data_32::pixel_type const * row = in.getRow(y);
				image_data_8::pixel_type  * row_out = out.getRow(y);
				for (unsigned x = 0; x < width; ++x)
				{
					unsigned val = row[x];
					byte index = 0;
					int idx = -1;

					//��������� ������� ����� 
					index = idx = tree.quantize(val);

					if (idx>=0 && idx<(int)alpha.size())
					{
						alpha[idx]+=U2ALPHA(val);
						alphaCount[idx]++;
					}
					//������ ������� ����� � ������� � �������
					row_out[x] = index;
				}
			}
			for(unsigned i=0; i<alpha.size(); i++)
			{
				if (alphaCount[i]!=0)
				{
					alpha[i] /= alphaCount[i];
				}
			}
		}

		void ss_reduce_8(image_data_32 const& in,
					  image_data_8 & out,
					  octree<rgb> &tree,
					  std::vector<unsigned> & alpha)
		{
			unsigned width = in.width();
			unsigned height = in.height();

			std::vector<unsigned> alphaCount(alpha.size());
			for(unsigned i=0; i<alpha.size(); i++)
			{
				alpha[i] = 0;
				alphaCount[i] = 0;
			}
			for (unsigned y = 0; y < height; ++y)
			{
				image_data_32::pixel_type const * row = in.getRow(y);
				image_data_8::pixel_type  * row_out = out.getRow(y);
				for (unsigned x = 0; x < width; ++x)
				{
					unsigned val = row[x];
					byte index = 0;
					int idx = -1;

					//��������� ������� ����� 
					index = idx = tree.ss_quantize(val);

					if (idx>=0 && idx<(int)alpha.size())
					{
						alpha[idx]+=U2ALPHA(val);
						alphaCount[idx]++;
					}
					//������ ������� ����� � ������� � �������
					if(index > 0)
						row_out[x] = index;
					else
						row_out[x] = 0;
				}
			}
			for(unsigned i=0; i<alpha.size(); i++)
			{
				if (alphaCount[i]!=0)
				{
					alpha[i] /= alphaCount[i];
				}
			}
		}


		void save_as_png(std::vector<rgb> const& palette,
						 image_data_8 const& image,
						 unsigned width,
						 unsigned height,
						 unsigned color_depth,
						 int compression,
						 int strategy,
						 std::vector<unsigned> const& alpha)
		{
			//png_voidp error_ptr=0;
			png_structp png_ptr=png_create_write_struct(PNG_LIBPNG_VER_STRING,
														NULL, &png_user_error_fn, &png_user_warning_fn);
			if (!png_ptr)
			{
				return;
			}

			png_infop info_ptr = png_create_info_struct(png_ptr);
			if (!info_ptr)
			{
				png_destroy_write_struct(&png_ptr,(png_infopp)0);
				return;
			}
			png_set_write_fn(png_ptr, NULL, this->write_data_fn, this->output_flush_fn);

			png_set_compression_level(png_ptr, compression);
			png_set_compression_strategy(png_ptr, strategy);

			png_set_IHDR(png_ptr, info_ptr, width, height, color_depth,
						 PNG_COLOR_TYPE_PALETTE,PNG_INTERLACE_NONE,
						 PNG_COMPRESSION_TYPE_DEFAULT,PNG_FILTER_TYPE_DEFAULT);

			png_color* pal = const_cast<png_color*>(reinterpret_cast<const png_color*>(&palette[0]));
			//���������� ���� �������
			png_set_PLTE(png_ptr, info_ptr, pal, palette.size());

			// make transparent lowest indexes, so tRNS is small
			if (alpha.size()>0)
			{
				std::vector<png_byte> trans(alpha.size());
				unsigned alphaSize=0;//truncate to nonopaque values
				for(unsigned i=0; i < alpha.size(); i++)
				{
					trans[i]=alpha[i];
					//����������� �������� 255, ������� ������������� �� ������������
					if (alpha[i]<255)
					{
						alphaSize = i+1;
					}
				}
				if (alphaSize>0)
				{
					//���������� ���� ����������������
					png_set_tRNS(png_ptr, info_ptr, (png_bytep)&trans[0], alphaSize, 0);
				}
			}

			png_write_info(png_ptr, info_ptr);
			for (unsigned i = 0; i < height; i++)
			{
				png_write_row(png_ptr,(png_bytep)image.getRow(i));
			}

			png_write_end(png_ptr, info_ptr);
			png_destroy_write_struct(&png_ptr, &info_ptr);
		}

		void save_as_32bpp_png(	image_data_32 const& image, 
								int compression = Z_DEFAULT_COMPRESSION,
								int strategy = Z_DEFAULT_STRATEGY)
		{
			png_structp png_ptr=png_create_write_struct(PNG_LIBPNG_VER_STRING,
														NULL, png_user_error_fn, png_user_warning_fn);

			if (!png_ptr)
			{
				return;
			}

			png_infop info_ptr = png_create_info_struct(png_ptr);
			if (!info_ptr)
			{
				png_destroy_write_struct(&png_ptr,(png_infopp)0);
				return;
			}

			png_set_write_fn(png_ptr, NULL, this->write_data_fn, this->output_flush_fn);

			png_set_compression_level(png_ptr, compression);
			png_set_compression_strategy(png_ptr, strategy);
			//png_set_compression_buffer_size(png_ptr, 32768);

			png_set_IHDR(png_ptr, info_ptr, 256, 256, 8,
						 PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE,
						 PNG_COMPRESSION_TYPE_DEFAULT,PNG_FILTER_TYPE_DEFAULT);

			png_write_info(png_ptr, info_ptr);
			for (unsigned i = 0; i < 256; i++)
			{
				png_write_row(png_ptr,(png_bytep)image.getRow(i));
			}

			png_write_end(png_ptr, info_ptr);
			png_destroy_write_struct(&png_ptr, &info_ptr);
		}


		void save_as_png8_oct(image_data_32 const& image,
							  const unsigned max_colors = 256,
							  int compression = Z_DEFAULT_COMPRESSION,
							  int strategy = Z_DEFAULT_STRATEGY)
		{
			// number of alpha ranges in png8 format; 2 results in smallest image with binary transparency
			// 3 is minimum for semitransparency, 4 is recommended, anything else is worse
			unsigned width = image.width();
			unsigned height = image.height();

			// octree table for separate alpha range with 1-based index (0 is fully transparent: no color)
			// �������� ������ � �������
			octree<rgb> tree;
			tree.setMaxColors(max_colors);
			//��������� �� ���� �������� � ��������� ���� � ������
			for (unsigned y = 0; y < height; ++y)
			{
				unsigned const * row = image.getRow(y);
				for (unsigned x = 0; x < width; ++x)
				{
					unsigned val = row[x]; //�������� argb
					// insert to proper tree based on alpha range
					tree.insert(rgb(U2RED(val), U2GREEN(val), U2BLUE(val)));
				}
			}
	
			std::vector<rgb> palette;
			palette.reserve(max_colors);

			tree.setMaxColors(max_colors);
			//���������� �������� �������
			tree.setOffset(palette.size());
			//������ ������� ��� j-�� ������������
			tree.ss_create_palette(palette);
			assert(palette.size() <= 256);

			//transparency values per palette index
			std::vector<unsigned> alphaTable;
			//������� ���������������� ��������������� �������
			alphaTable.resize(palette.size());//allow semitransparency also in almost opaque range

			// >16 && <=256 colors -> write 8-bit color depth
			image_data_8 reduced_image(width,height);
			ss_reduce_8(image, reduced_image, tree, alphaTable);
			save_as_png(palette, reduced_image, width, height, 8, compression, strategy, alphaTable);
		}

		bool reduceTo8bpp(image_data_32 const& in, std::vector<rgb> *palette, std::vector<unsigned> *alpha, png_bytep *row_pointers)
		{
			png_uint_32 width = 256;
			png_uint_32 height = 256;
			//int bit_depth;
			//int color_type;
			//int interlace_type;
			//std::vector<rgb> colors_palette;

			//png_byte *image = new png_byte[width * height *4];
			//png_byte *row;
			//png_byte r, g, b, a;
			std::vector<rgbn> color_entries;
			std::vector<rgbn>::iterator it1;
			png_byte* row_in = (png_bytep)in.getRow(0);
			for (unsigned int i = 0; i < 256 * 256; i++)
			{
				unsigned b = *(row_in + i * 4);
				unsigned g = *(row_in + i * 4 + 1);
				unsigned r = *(row_in + i * 4 + 2);
				unsigned a = *(row_in + i * 4 + 3);

				rgbn entry = rgbn(r, g, b, 1);
				if ((it1 = std::find(color_entries.begin(), color_entries.end(), entry)) != color_entries.end()) 
				{
					it1->n++;
				} 
				else 
				{
					color_entries.push_back(entry);
				}
			}


			/* ���������� ������ �� ������� ������������� */
			std::sort(color_entries.begin(), color_entries.end(), node_cmp_by_count());

			int cl = color_entries.size() > 256 ? 256 : color_entries.size();
			for(unsigned i = 0; i < cl; i++)
			{
				rgbn item = color_entries[i];
				palette->push_back(rgb(item.r, item.g, item.b));
			}
			
			//������� ���������������� ��������������� �������
			alpha->resize(palette->size());//allow semitransparency also in almost opaque range
			std::vector<unsigned> alphaCount(alpha->size());
			for(unsigned i=0; i < alpha->size(); i++)
			{
				alpha->at(i) = 0;
				alphaCount[i] = 0;
			}

			/* �������� DAT chanks ��� 8bpp */
			/* ��������� ������ �������� ����� */
			for (int row = 0; row < 256; row++)
				row_pointers[row] = NULL;

			for (int row = 0; row < 256; row++)
				row_pointers[row] = (png_bytep)malloc(256);
			
			std::vector<rgb>::iterator it2;
			for (unsigned int y = 0; y < 256; y++)
			{
				for (unsigned int x = 0; x < 256; x++)
				{
					int i = y * 256 + x;
					unsigned b = *(row_in + i * 4);
					unsigned g = *(row_in + i * 4 + 1);
					unsigned r = *(row_in + i * 4 + 2);
					unsigned a = *(row_in + i * 4 + 3);

					rgb entry = rgb(r, g, b);
					size_t index;
					if ((it2 = std::find(palette->begin(), palette->end(), entry)) != palette->end()) 
					{
						index = std::distance(palette->begin(), it2);
						*(row_pointers[y] + x) = index;

						if (index >= 0 && index < (int)alpha->size())
						{
							alpha->at(index) += a;
							alphaCount[index]++;
						}
					}
					else
					{
						unsigned minDiff = 4294967295;
						for (int i = 0; i < palette->size(); i++)
						{
							unsigned currentDiff = GetMaxDiff(entry, palette->at(i));
							if (currentDiff < minDiff)
							{
								minDiff = currentDiff;
								index = i;
							}
						}

						*(row_pointers[y] + x) = index;
						if (index >= 0 && index < (int)alpha->size())
						{
							alpha->at(index) += a;
							alphaCount[index]++;
						}
					}
				}
			}

			for(unsigned i=0; i<alpha->size(); i++)
			{
				if (alphaCount[i]!=0)
				{
					alpha->at(i) /= alphaCount[i];
				}
			}

			//for(unsigned i = 0; i < color_entries.size(); i++)
			//{
			//	delete &color_entries[i];
			//}
			color_entries.clear();
			alphaCount.clear();

			return true;
		}	

		bool write8bppRGB(std::vector<rgb> const& palette, std::vector<unsigned> const& alpha, png_bytep *row_pointers)
		{
			png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, &png_user_error_fn, &png_user_warning_fn);
			if (png_ptr == NULL) {
				return false;
			}

			png_infop info_ptr = png_create_info_struct(png_ptr);
			if (info_ptr == NULL) {
				png_destroy_write_struct(&png_ptr,  NULL);
				return false;
			}

			png_set_write_fn(png_ptr, NULL, this->write_data_fn, this->output_flush_fn);

			png_set_IHDR(png_ptr, info_ptr, 256, 256, 8, PNG_COLOR_TYPE_PALETTE,
				PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

			png_color* pal = const_cast<png_color*>(reinterpret_cast<const png_color*>(&palette[0]));

			if(palette.size() > 0)
			{
				// make transparent lowest indexes, so tRNS is small
				if (alpha.size()>0)
				{
					std::vector<png_byte> trans(alpha.size());
					unsigned alphaSize=0;//truncate to nonopaque values
					for(unsigned i=0; i < alpha.size(); i++)
					{
						trans[i]=alpha[i];
						//����������� �������� 255, ������� ������������� �� ������������
						if (alpha[i]<255)
						{
							alphaSize = i+1;
						}
					}
					if (alphaSize>0)
					{
						//���������� ���� ����������������
						png_set_tRNS(png_ptr, info_ptr, (png_bytep)&trans[0], alphaSize, 0);
					}
				}


				png_set_PLTE(png_ptr, info_ptr, pal, palette.size());
				png_write_info(png_ptr, info_ptr);
				png_write_image(png_ptr, row_pointers);
				png_write_end(png_ptr, info_ptr);
				png_destroy_write_struct(&png_ptr, &info_ptr);
				return true; 
			}
			png_destroy_write_struct(&png_ptr, &info_ptr);

			return false;
		}

		unsigned GetMaxDiff(rgb a, rgb b)
        {
            unsigned bDiff = a.b > b.b ? a.b - b.b : b.b - a.b;

			unsigned gDiff = a.g > b.g ? a.g - b.g : b.g - a.g;

            unsigned rDiff = a.r > b.r ? a.r - b.r : b.r - a.r;

            byte max = bDiff > gDiff ? bDiff : gDiff;

            max = max > rDiff ? max : rDiff;

            return max;
        }

	private:
		png_rw_ptr write_data_fn;
		png_flush_ptr output_flush_fn;
};
#endif